package com.example.putex.myapplication;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity
public class Carta implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String image;
    private String name;
    private String type;
    private String rarity;
    private String color;

    public Carta(){

    }

    public Carta(String image, String name, String type, String rarity, String color) {
        this.image = image;
        this.name = name;
        this.type = type;
        this.rarity = rarity;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public String toString() {
        return "Carta{" +
                "image='" + image + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", rarity='" + rarity + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
