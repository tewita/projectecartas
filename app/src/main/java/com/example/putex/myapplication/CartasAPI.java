package com.example.putex.myapplication;

import android.net.Uri;

import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class CartasAPI {
    private final int pages = 5;
    private final String BASE_URL = "https://api.magicthegathering.io/v1/cards";

//    ArrayList<Carta> getName(String color) {
////        Uri builtUri = Uri.parse(BASE_URL)
////                .buildUpon()
////                /**AppendPath '/'*/
////                /**AppendQuery '?'*/
////                .build();
////        String url = builtUri.toString();
////
////        return doCall(url);
////    }
//
//    ArrayList<Carta> getColor(String color) {
//        Uri builtUri = Uri.parse(BASE_URL)
//                .buildUpon()
//                .appendQueryParameter("colors", color)
//                /**AppendPath '/'*/
//                /**AppendQuery '?'*/
//                .build();
//        String url = builtUri.toString();
//
//        return doCall(url);
//    }
//    ArrayList<Carta> getrarity(String rarity) {
//        Uri builtUri = Uri.parse(BASE_URL)
//                .buildUpon()
//                .appendQueryParameter("rarity", rarity)
//                .build();
//        String url = builtUri.toString();
//
//        return doCall(url);
//    }

    ArrayList<Carta> getRarityColor(String color, String rarity) {


        return doCall(color, rarity);

    }


    private String getUrlPage(String color,String rarity){

        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("colors", color)
                .appendQueryParameter("rarity", rarity)
                .build();
    return builtUri.toString();

    }
    private ArrayList<Carta> doCall(String color,String rarity){
        ArrayList<Carta> carta= new ArrayList<>();
        for (int i = 0; i < pages; i++) {
            try {
                String url = getUrlPage(color,rarity);
                String JsonResponse = HttpUtils.get(url);
                ArrayList<Carta> list = processJson(JsonResponse);
                carta.addAll(list);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return carta;

    }

    private ArrayList<Carta> processJson(String jsonResponse) {
        ArrayList<Carta> cartas = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            /**'cards' lo coge de la api, con otro nombre fallaria*/
            JSONArray jsonCartas = data.getJSONArray("cards");
            for (int i = 0; i < jsonCartas.length(); i++) {
                JSONObject jsonCarta = jsonCartas.getJSONObject(i);

                Carta carta = new Carta();
                /**Aqui pasa exactamente como antes, el get.String hay que poner
                 * los nombres de cada valor por la api.
                 */
                carta.setName(jsonCarta.getString("name"));
                carta.setRarity(jsonCarta.getString("rarity"));
                carta.setType(jsonCarta.getString("type"));
                carta.setColor(jsonCarta.getString("colors"));
                carta.setImage(jsonCarta.getString("imageUrl"));


                cartas.add(carta);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cartas;
    }

}

