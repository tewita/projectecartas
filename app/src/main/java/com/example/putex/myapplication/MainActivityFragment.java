package com.example.putex.myapplication;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.AdapterView;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;


import android.view.MenuItem;

import android.os.AsyncTask;
import android.app.ProgressDialog;



/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    private ArrayList<Carta> items;
    private CartasAdapter adapter;
    private SharedPreferences preferences;
    private CartasViewModel model;
    private SharedViewModel sharedModel;
    private ProgressDialog dialog;

    public MainActivityFragment() {

    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    boolean esTablet(){
        return getResources().getBoolean(R.bool.tablet);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ListView lvCartas = (ListView) view.findViewById(R.id.lvCartas);


        items = new ArrayList<>();

        adapter = new CartasAdapter(
                getContext(),
                R.layout.lv_cartas_row,
                items
        );

        sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        lvCartas.setAdapter(adapter);

        lvCartas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Carta carta = (Carta) adapterView.getItemAtPosition(i);

                if (!esTablet()) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("carta", carta);
                    startActivity(intent);
                }
                else{
                    sharedModel.select(carta);
                }
            }
        });

        model = ViewModelProviders.of(this).get(CartasViewModel.class);
        model.getCartas().observe(this, new Observer<List<Carta>>() {
            @Override
            public void onChanged(@Nullable List<Carta> carta) {
                adapter.clear();
                adapter.addAll(carta);
            }
        });

        model = ViewModelProviders.of(this).get(CartasViewModel.class);
        model.getCartas().observe(this, new Observer<List<Carta>>() {
            @Override
            public void onChanged(@Nullable List<Carta> cartas) {
                adapter.clear();
                adapter.addAll(cartas);
            }
        });

        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading...");

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if(mostrat)
                    dialog.show();
                else
                    dialog.dismiss();
            }
        });


        return view;

    }
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cartas_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }


    private void refresh() {

        model.reload();

    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Carta>> {
        @Override
        protected ArrayList<Carta> doInBackground(Void... voids) {
            CartasAPI api = new CartasAPI();

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String color = preferences.getString("color", "");
            String rarity = preferences.getString("rarity", "");

            ArrayList<Carta> result = api.getRarityColor(color,rarity);
            Log.d("DEBUG", result.toString());

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Carta> cartas) {

            adapter.clear();
            for (Carta card : cartas) {
                adapter.add(card);


            }

        }


    }
}
