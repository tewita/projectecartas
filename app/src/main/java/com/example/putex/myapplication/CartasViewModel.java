package com.example.putex.myapplication;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;



public class CartasViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CartaDao cartaDao;
    private LiveData<List<Carta>> cartas;
    private MutableLiveData<List<Carta>> carta;
    private static final int pages=10;
    private MutableLiveData<Boolean> loading;

    public CartasViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.cartaDao = appDatabase.getCartaDao();

    }

    public LiveData<List<Carta>> getCartas() {
        Log.d("DEBUG", "ENTRA");

     return cartaDao.getCartas();
    }

    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }
    public MutableLiveData<Boolean> getLoading() {
        if(loading == null){
            loading = new MutableLiveData<>();
        }
        return loading;
    }


    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Carta>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<Carta> doInBackground(Void... voids) {

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );
            CartasAPI api = new CartasAPI();

            String color = preferences.getString("color", "");
            String rarity = preferences.getString("rarity", "");

            ArrayList<Carta> result = api.getRarityColor(color, rarity);

            Log.d("DEBUG", result != null ? result.toString() : null);
                cartaDao.deleteCartas();
                cartaDao.addMovies(result);
            return result;
        }
        protected void onPostExecute(ArrayList<Carta> cartas) {
            super.onPostExecute(cartas);
            loading.setValue(false);
        }


    }
}
