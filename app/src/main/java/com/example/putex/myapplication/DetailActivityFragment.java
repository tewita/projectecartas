package com.example.putex.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment  extends Fragment {
    private View view;
    private ImageView ivPosterImage;
    private TextView lvCarta;
    private TextView lvColor;
    private TextView lvRarity;
    private TextView lvCriticas;
    private TextView lvDescripcion;




    public DetailActivityFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail, container, false);

        Intent i = getActivity().getIntent();

        if (i != null) {
            Carta carta = (Carta) i.getSerializableExtra("carta");

            if (carta != null) {
                updateUi(carta);
            }
        }
        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);
        sharedModel.getSelected().observe(this, new Observer<Carta>() {
            @Override
            public void onChanged(@Nullable Carta carta) {
                updateUi(carta);
            }
        });


        return view;
    }

    private void updateUi(Carta carta) {
        Log.d("CARTA", carta.toString());
        ivPosterImage = (ImageView) view.findViewById(R.id.ivPosterImage);
        lvCarta = (TextView) view.findViewById(R.id.lvCarta);
        lvColor = (TextView) view.findViewById(R.id.lvColor);
        lvRarity = (TextView) view.findViewById(R.id.lvRarity);
        lvCriticas = (TextView) view.findViewById(R.id.lvCriticas);
        lvDescripcion = (TextView) view.findViewById(R.id.lvDescripcion);

        lvCarta.setText(carta.getName());
        lvColor.setText(carta.getColor());
        lvRarity.setText(carta.getRarity());
        lvDescripcion.setText(carta.getType());
        Glide.with(getContext()).load(
                "" + carta.getImage()
        ).into(ivPosterImage);



    }

}

