package com.example.putex.myapplication;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by carlesgm on 29/8/17.
 */

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Carta> selected = new MutableLiveData<Carta>();

    public void select(Carta carta) {
        selected.setValue(carta);
    }

    public LiveData<Carta> getSelected() {
        return selected;
    }
}
